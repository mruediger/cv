(package-initialize)
(require 'org)
(require 'ox)

(add-to-list 'org-latex-classes '("mycv"
                                  "\\documentclass{mycv}
                                     [NO-DEFAULT-PACKAGES]
                                     [EXTRA]"
                                  ("\\section{%s}" . "\\section*{%s}")
                                  ("\\subsection{%s}" . "\\subsection*{%s}")
                                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
                                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(org-latex-export-to-latex)
