\ProvidesClass{mycv}
\NeedsTeXFormat{LaTeX2e}
\LoadClass[a4paper]{article}

\RequirePackage{xcolor}
\RequirePackage{parskip}
\RequirePackage{hyperref}
\RequirePackage[default,opentype]{sourcesanspro}
\RequirePackage[defaultsans]{droidsans}
\RequirePackage{colortbl}

\newcommand*{\address}[1]{\def\@address{#1}}
\newcommand*{\phone}[1]{\def\@phone{#1}}
\newcommand*{\email}[1]{\def\@email{#1}}

%-------------------------------------------------------------------------------
% page settings
%-------------------------------------------------------------------------------

%https://en.wikibooks.org/wiki/LaTeX/Lengths

\newlength\cvmargin
\setlength{\cvmargin}{2cm}
\setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-2\cvmargin}

\setlength{\oddsidemargin}{\cvmargin}
\addtolength{\oddsidemargin}{-1in}

\setlength{\evensidemargin}{\oddsidemargin}
\setlength{\headheight}{0cm}
\setlength{\textheight}{\paperheight}
\addtolength{\textheight}{-\headheight}
\addtolength{\textheight}{-\headsep}
\addtolength{\textheight}{-\footskip}
\addtolength{\textheight}{-1.5\cvmargin}

\setlength{\topmargin}{\cvmargin}
\addtolength{\topmargin}{-1in}

\newlength\boxwidth
\setlength{\boxwidth}{\textwidth}
\addtolength{\boxwidth}{-6pt}

%reduce list spacing
\setlength{\parskip}{2pt}

%reduce table padding
\setlength{\tabcolsep}{0pt}

\pagestyle{empty}

%-------------------------------------------------------------------------------
% style settings
%-------------------------------------------------------------------------------

\definecolor{orange}{RGB}{255, 85, 0}
\definecolor{addresscolor}{rgb}{0,0,0}

\newcommand{\addressfont}[1]{{\normalsize\sffamily\mdseries\upshape {\color{addresscolor} #1 }}}

\renewcommand{\maketitle}{
  {\Huge\@author}{\color{orange}\hrule}
  \@address \hspace{1em} \@phone \hspace{1em} \@email
}

\renewcommand{\section}[1]{%
  \vspace{5mm}%
  \par%
  \colorbox{orange}{%
    \begin{minipage}{\boxwidth}%
      {\Large\mdseries\upshape\textcolor{white}{#1}}%
  \end{minipage}}%
}

\newcommand*{\jobtitle}[1]{{\bfseries #1}}
\newcommand*{\company}[1]{#1}%
\newcommand*{\location}[1]{#1}%
\newcommand*{\period}[1]{#1}%

\def\splitsubsectionX#1|#2|#3|#4\relax{%
  \par
  \jobtitle{#1} \hfill \location{#3}%
  \noindent{\color{orange}\rule[2ex]{\textwidth}{.5pt}}%
  \vspace{-2.5ex}%
  \company{#2} \hfill \period{#4}%
  \par%
}

\def\splitsubsection#1|#2|#3|#4\relax{%
  \par%
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lr}
    \jobtitle{#1} & \location{#3}\\
    \arrayrulecolor{orange}\hline
    \company{#2} &  \period{#4}\\
  \end{tabular*}
}

\renewcommand{\subsection}[1]{\splitsubsection #1\relax}
