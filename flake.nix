{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs: with inputs;
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          (import emacs-overlay)
        ];
      };

      myNativeBuildInputs = with pkgs; [
        (emacsWithPackagesFromUsePackage {
          alwaysEnsure = true;
          config = "";
          package = pkgs.emacs;
          extraEmacsPackages = epkgs: with epkgs; [
            org
          ];
        })

        (texliveSmall.withPackages (
          ps: with ps; [
            marvosym
            droid
            xurl
            enumitem
            sectsty
            xstring
            xifthen
            ifmtarg
            lualatex-math
            roboto
            fontawesome5
            sourcesanspro
            tcolorbox
            environ
            tikzfill
            wrapfig
            capt-of
          ]
        ))
      ];
      name = "cv";
    in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = myNativeBuildInputs;
        };

        packages.${system} = {
          default = pkgs.stdenv.mkDerivation rec {
            inherit system name;
            nativeBuildInputs = myNativeBuildInputs;
            src = ./.;
            buildPhase = ''
              export HOME=$(pwd)
              emacs --batch cv.org -l org-export.el
              lualatex cv.tex
            '';
            installPhase = ''
              find
              cp -r . $out
            '';
          };
        };
      };
}
